# Backends for LB

Simple backend: https://gitlab.com/advu/simple_backend

Run:

```bash
docker-compose up -d
```

Stop:

```bash
docker-compose down
```

Stop with cleaning data:
```bash
docker-compose down --volumes
```

Web:

- Grafana: http://localhost:3000/
- Prometheus: http://localhost:9090/
- Backend №1: http://localhost:8080/
- Backend №2: http://localhost:8081/
- Backend №3: http://localhost:8082/
