# CHANGELOG

## 0.1.0

1. Three simple backends.
2. Prometheus with scraping metrics from backends.
3. Grafana with datasource configuration and dashboard.
